public class BadStudent extends Student implements Executable{
    private String deductionOrder;
    public BadStudent(String name, int age, String faculty, int year, String deductionOrder){
        super(name, age, faculty, year);
        this.deductionOrder = deductionOrder;
    }

    @Override
    public void execute() {
        System.out.println("Execute");
    }
}
