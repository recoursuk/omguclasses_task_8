public class Student extends Human {
    private String faculty;
    private int year;
    public Student(String name, int age, String faculty, int year){
        super(name, age);
        this.faculty = faculty;
        this.year = year;
    }
}
