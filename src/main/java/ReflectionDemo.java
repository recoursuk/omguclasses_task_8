import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {

    public static int countOfHumans(List<Object> list){
        int res = 0;
        for(Object e:list){
            if(e instanceof Human){
                res++;
            }
        }
        return res;
    }

    public static List<String> getListOfPublicMethods(Object object){
        List<String> res = new ArrayList<String>();
        for(Method e:object.getClass().getMethods()){
            res.add(e.getName());
        }
        return res;
    }

    public static List<String> getListOfAllSuperclasses(Object object){
        List<String> res = new ArrayList<String>();
        Class<?> changer = object.getClass();
        while (changer.getSuperclass() != null){
            changer = changer.getSuperclass();
            res.add(changer.getName());
        }
        return res;
    }

    public static List<String> getSettersList(Object object){//5
        List<String> res = new ArrayList<>();
        Class<?> objectClass = object.getClass();
        Method[] objectMethods = objectClass.getDeclaredMethods();
        for (Method m: objectMethods){
            if(m.getReturnType() == void.class &&
                    !Modifier.isStatic(m.getModifiers()) &&
                    Modifier.isPublic(m.getModifiers()) &&
                    m.getName().startsWith("set") &&
                    m.getParameterCount() == 1
            ){
                res.add(m.getName());
            }
        }
        return res;
    }

    public static List<String> getGettersList(Object object){//5
        List<String> res = new ArrayList<>();
        Class<?> objectClass = object.getClass();
        Method[] objectMethods = objectClass.getDeclaredMethods();
        for (Method m: objectMethods){
            if(m.getReturnType() != void.class &&
                    !Modifier.isStatic(m.getModifiers()) &&
                    Modifier.isPublic(m.getModifiers()) &&
                    m.getName().startsWith("get") &&
                    m.getParameterCount() == 0
            ){
                res.add(m.getName());
            }
        }
        return res;
    }

    public static int getAmountOfExecObjects(Object[] objects) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {//4
        int count = 0;
        for (Object obj : objects) {
            if (obj instanceof Executable) {
                Method met = obj.getClass().getMethod("execute");
                count++;
                met.invoke(obj);
            }
        }
        return count;
    }
}
