import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class TestReflections {
    @Test
    public void testAmountOfHumanClassInList(){
        ArrayList objects = new ArrayList();
        objects.add(new Car(500, "1234", "Green"));
        objects.add(new Human("K",23));
        objects.add(3);
        objects.add(55.0);

        Assert.assertEquals(1, ReflectionDemo.countOfHumans(objects));
    }

    @Test
    public void testGetMethodsForObject(){
        Car car = new Car(500, "1234", "green");

        for (String s: ReflectionDemo.getListOfPublicMethods(car)){
            System.out.println(s);
        }
    }

    @Test
    public void testGetAllSuperClasses() throws IOException {
        BadStudent bd = new BadStudent("1", 19, "IMIT", 1, "Erased)))");
        for (String s: ReflectionDemo.getListOfAllSuperclasses(bd)){
            System.out.print(" -> " + s);
        }
    }

    @Test
    public void testGetAmountOfExecObjects() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Object[] objects = new Object[]{
                //new Car(500, "1234", "Green"),
                new BadStudent("1", 19, "IMIT", 1, "Erased)))"),
                new Human("Kitos", 23),
                new Car(33, "44343j", "blue"),
                3, 55.0
        };

        Assert.assertEquals(2, ReflectionDemo.getAmountOfExecObjects(objects));
    }

    @Test
    public void testGetSettersList(){
        Car car = new Car(500, "1234", "green");

        for (String s: ReflectionDemo.getSettersList(car)){
            System.out.println(s);
        }
    }

    @Test
    public void testGetGettersList(){
        Human car = new Human("Kitos", 23);

        for (String s: ReflectionDemo.getGettersList(car)){
            System.out.println(s);
        }
    }


}
